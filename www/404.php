<?php
include_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/urlrewrite.php');
CHTTP::SetStatus("404 Not Found");
@define("ERROR_404","Y");
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
CBXShortUri::CheckUri();
$APPLICATION->AddChainItem('404', "");
$APPLICATION->SetTitle("Страница не найдена");
?>
<h1>Страница не найдена</h1>
<p>Запрошенная вами страница отсутствует на сайте. Возможно она была удалена или перемещена. Попробуйте перейти на <a href="/">главную страницу</a> или воспользуйтесь картой сайта.
</p>
<h2>Карта сайта</h2>
<?$APPLICATION->IncludeComponent(
	"bitrix:main.map",
	".default",
	array(
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "3600",
		"SET_TITLE" => "N",
		"LEVEL" => "3",
		"COL_NUM" => "1",
		"SHOW_DESCRIPTION" => "N"
	),
	false
);?>
<?php
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");
?>
