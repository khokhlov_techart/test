<?php return array(
    'root' => array(
        'pretty_version' => '1.0.0+no-version-set',
        'version' => '1.0.0.0',
        'type' => 'library',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'reference' => NULL,
        'name' => 'techart/project',
        'dev' => true,
    ),
    'versions' => array(
        'doctrine/inflector' => array(
            'pretty_version' => '1.4.4',
            'version' => '1.4.4.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../doctrine/inflector',
            'aliases' => array(),
            'reference' => '4bd5c1cdfcd00e9e2d8c484f79150f67e5d355d9',
            'dev_requirement' => false,
        ),
        'illuminate/container' => array(
            'pretty_version' => 'v5.8.36',
            'version' => '5.8.36.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../illuminate/container',
            'aliases' => array(),
            'reference' => 'b42e5ef939144b77f78130918da0ce2d9ee16574',
            'dev_requirement' => false,
        ),
        'illuminate/contracts' => array(
            'pretty_version' => 'v5.8.36',
            'version' => '5.8.36.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../illuminate/contracts',
            'aliases' => array(),
            'reference' => '00fc6afee788fa07c311b0650ad276585f8aef96',
            'dev_requirement' => false,
        ),
        'illuminate/events' => array(
            'pretty_version' => 'v5.8.36',
            'version' => '5.8.36.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../illuminate/events',
            'aliases' => array(),
            'reference' => 'a85d7c273bc4e3357000c5fc4812374598515de3',
            'dev_requirement' => false,
        ),
        'illuminate/filesystem' => array(
            'pretty_version' => 'v5.8.36',
            'version' => '5.8.36.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../illuminate/filesystem',
            'aliases' => array(),
            'reference' => '494ba903402d64ec49c8d869ab61791db34b2288',
            'dev_requirement' => false,
        ),
        'illuminate/support' => array(
            'pretty_version' => 'v5.8.36',
            'version' => '5.8.36.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../illuminate/support',
            'aliases' => array(),
            'reference' => 'df4af6a32908f1d89d74348624b57e3233eea247',
            'dev_requirement' => false,
        ),
        'illuminate/view' => array(
            'pretty_version' => 'v5.8.36',
            'version' => '5.8.36.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../illuminate/view',
            'aliases' => array(),
            'reference' => 'c859919bc3be97a3f114377d5d812f047b8ea90d',
            'dev_requirement' => false,
        ),
        'nesbot/carbon' => array(
            'pretty_version' => '2.53.1',
            'version' => '2.53.1.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../nesbot/carbon',
            'aliases' => array(),
            'reference' => 'f4655858a784988f880c1b8c7feabbf02dfdf045',
            'dev_requirement' => false,
        ),
        'nilportugues/sitemap-component' => array(
            'pretty_version' => '3.0.3',
            'version' => '3.0.3.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../nilportugues/sitemap-component',
            'aliases' => array(),
            'reference' => 'd9986472b8b7c9767e5ee356624fd1ee1ba0f239',
            'dev_requirement' => false,
        ),
        'psr/container' => array(
            'pretty_version' => '1.1.1',
            'version' => '1.1.1.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../psr/container',
            'aliases' => array(),
            'reference' => '8622567409010282b7aeebe4bb841fe98b58dcaf',
            'dev_requirement' => false,
        ),
        'psr/log' => array(
            'pretty_version' => '1.1.4',
            'version' => '1.1.4.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../psr/log',
            'aliases' => array(),
            'reference' => 'd49695b909c3b7628b6289db5479a1c204601f11',
            'dev_requirement' => false,
        ),
        'psr/simple-cache' => array(
            'pretty_version' => '1.0.1',
            'version' => '1.0.1.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../psr/simple-cache',
            'aliases' => array(),
            'reference' => '408d5eafb83c57f6365a3ca330ff23aa4a5fa39b',
            'dev_requirement' => false,
        ),
        'symfony/debug' => array(
            'pretty_version' => 'v4.4.31',
            'version' => '4.4.31.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/debug',
            'aliases' => array(),
            'reference' => '43ede438d4cb52cd589ae5dc070e9323866ba8e0',
            'dev_requirement' => false,
        ),
        'symfony/deprecation-contracts' => array(
            'pretty_version' => 'v2.4.0',
            'version' => '2.4.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/deprecation-contracts',
            'aliases' => array(),
            'reference' => '5f38c8804a9e97d23e0c8d63341088cd8a22d627',
            'dev_requirement' => false,
        ),
        'symfony/finder' => array(
            'pretty_version' => 'v4.4.30',
            'version' => '4.4.30.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/finder',
            'aliases' => array(),
            'reference' => '70362f1e112280d75b30087c7598b837c1b468b6',
            'dev_requirement' => false,
        ),
        'symfony/polyfill-ctype' => array(
            'pretty_version' => 'v1.23.0',
            'version' => '1.23.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/polyfill-ctype',
            'aliases' => array(),
            'reference' => '46cd95797e9df938fdd2b03693b5fca5e64b01ce',
            'dev_requirement' => false,
        ),
        'symfony/polyfill-mbstring' => array(
            'pretty_version' => 'v1.23.1',
            'version' => '1.23.1.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/polyfill-mbstring',
            'aliases' => array(),
            'reference' => '9174a3d80210dca8daa7f31fec659150bbeabfc6',
            'dev_requirement' => false,
        ),
        'symfony/polyfill-php80' => array(
            'pretty_version' => 'v1.23.1',
            'version' => '1.23.1.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/polyfill-php80',
            'aliases' => array(),
            'reference' => '1100343ed1a92e3a38f9ae122fc0eb21602547be',
            'dev_requirement' => false,
        ),
        'symfony/translation' => array(
            'pretty_version' => 'v5.3.9',
            'version' => '5.3.9.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/translation',
            'aliases' => array(),
            'reference' => '6e69f3551c1a3356cf6ea8d019bf039a0f8b6886',
            'dev_requirement' => false,
        ),
        'symfony/translation-contracts' => array(
            'pretty_version' => 'v2.4.0',
            'version' => '2.4.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/translation-contracts',
            'aliases' => array(),
            'reference' => '95c812666f3e91db75385749fe219c5e494c7f95',
            'dev_requirement' => false,
        ),
        'symfony/translation-implementation' => array(
            'dev_requirement' => false,
            'provided' => array(
                0 => '2.3',
            ),
        ),
        'techart/bitrix.tao' => array(
            'pretty_version' => '1.15.0',
            'version' => '1.15.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../techart/bitrix.tao',
            'aliases' => array(),
            'reference' => '2ca14e01a340e3b06dd52deb64dd9a77f99a7314',
            'dev_requirement' => false,
        ),
        'techart/frontend-api' => array(
            'pretty_version' => '3.1.4',
            'version' => '3.1.4.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../techart/frontend-api',
            'aliases' => array(),
            'reference' => '73efb2c7fd156ae23e18bb24d7a4b7b957ee95c7',
            'dev_requirement' => false,
        ),
        'techart/project' => array(
            'pretty_version' => '1.0.0+no-version-set',
            'version' => '1.0.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'reference' => NULL,
            'dev_requirement' => false,
        ),
        'twig/twig' => array(
            'pretty_version' => 'v1.44.5',
            'version' => '1.44.5.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../twig/twig',
            'aliases' => array(),
            'reference' => 'dd4353357c5a116322e92a00d16043a31881a81e',
            'dev_requirement' => false,
        ),
    ),
);
