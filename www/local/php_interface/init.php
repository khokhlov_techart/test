<?php
include_once($_SERVER['DOCUMENT_ROOT'].'/local/vendor/autoload.php');
include_once (__DIR__ . '/lib/Helpers.php');

if(class_exists('\TAO')) {
	\TAO::Init([
		'auth' => 'http://office.techart.ru/project11393/siteadmin/',
		'infoblock.schema.delete' => false,
	]);
}
