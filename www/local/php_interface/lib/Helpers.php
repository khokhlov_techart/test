<?php

class Helpers
{
	public static function cropText($text, $limit)
	{
		$cropped_text = mb_substr($text, 0, $limit);
		if (mb_strlen($cropped_text) < mb_strlen($text)) {
			if (!in_array(mb_substr($text, $limit, 1), array(' ', '?', '!', '.', ','))) {
				$limit = mb_strripos($cropped_text, ' ');
				$cropped_text = mb_substr($cropped_text, 0, $limit);
			}
			$cropped_text = trim($cropped_text, ' !?.,');
			$cropped_text .= '...';
		}
		return $cropped_text;
	}

	public static function isMain()
	{
		return \TAO::app()->GetCurPage(false) === SITE_DIR;
	}

	public static function isLocal()
	{
		return in_array(\TAO::getOption('env'), ['dev', 'hot']);
	}

	public static function addPageContainerIfNecessary()
	{
		global $APPLICATION;
		if (!self::isMain() && !$APPLICATION->GetProperty("WITHOUT_CONTAINER")) {
			$block = \TAO::frontend()->block('layout');
			return '<div class="' . $block->elem('page-container') . '">';
		}
	}

	public static function closePageContainerIfNecessary()
	{
		global $APPLICATION;
		if(!self::isMain() && !$APPLICATION->GetProperty("WITHOUT_CONTAINER")) {
			return '</div>';
		}
	}

	public static function putSvgImageFromFrontend($svgPathInfFrontend)
	{
		return file_get_contents(
			\TAO::rootDir(
				rtrim(\TAO::app()->GetTemplatePath('frontend'), '/')
				. '/'
				. ltrim($svgPathInfFrontend, '/')
			)
		);
	}

	public static function getPhoneHref($phone)
	{
		$phone = preg_replace('/\D/', '', $phone);
		if ($phone[0] === '8' || $phone[0] === '7') {
			$phone = substr($phone, 1);
		}
		return 'tel:+7' . $phone;
	}
}
