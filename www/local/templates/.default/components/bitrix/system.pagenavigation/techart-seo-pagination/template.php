<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$this->setFrameMode(true);

if(!$arResult["NavShowAlways"])
{
	if ($arResult["NavRecordCount"] == 0 || ($arResult["NavPageCount"] == 1 && $arResult["NavShowAll"] == false))
		return;
}
?>

<?php

	// чистим урл от пагинации
	$arResult["sUrlPath"] = preg_replace("/\/page-([\d]+|all)\//is", "/", $arResult["sUrlPath"]);
	$strNavQueryStringFull = ($arResult["NavQueryString"] != "" ? "?".$arResult["NavQueryString"] : "");
	$pageName = 'page-';
	$block = \TAO::frontend()->block('page-navigation');
?>

<?php
	$fullUrl = (isset($_SERVER['HTTPS']) ? 'https' : 'http') .'://' . $_SERVER['HTTP_HOST'] . $arResult["sUrlPath"];
	$prev = false;
	$next = false;
	$prevUrl = '';
	$nextUrl = '';

	if($arResult['NavPageNomer'] > 2) {
		$prevUrl = $fullUrl . $pageName . ($arResult['NavPageNomer'] - 1) . '/';
		$prev = true;
	} else if ($arResult['NavPageNomer'] == 2) {
		$prevUrl = $fullUrl;
		$prev = true;
	}

	if($arResult['NavPageNomer'] != $arResult['NavPageCount']) {
		$nextUrl = $fullUrl . $pageName . ($arResult['NavPageNomer'] + 1) . '/';
		$next = true;
	}

	if ($arResult["bShowAll"]) {
		$APPLICATION->SetPageProperty('canonical', $fullUrl.$pageName.'all/');
	} else {
		$APPLICATION->SetPageProperty('canonical', $fullUrl);
	}
?>

<div class="<?= $block ?>">
	<?php // предыдущий ?>
	<?php if ($arResult['NavPageNomer'] != 1) { ?>
		<a
			class="<?= $block->elem('page')->mod('prev') ?>"
			href="<?= $arResult["sUrlPath"] ?><?= $arResult['NavPageNomer'] == 2 ? '' : $pageName.($arResult["NavPageNomer"] - 1).'/' ?><?= $strNavQueryStringFull ?>"
		>
			<?=GetMessage("nav_prev")?>
		</a>
	<?php } ?>
	<?php // первая страница ?>
	<?php if ($arResult["nStartPage"] != 1) { ?>
		<a
			class="<?= $block->elem('page') ?>"
			href="<?= $arResult["sUrlPath"] ?><?= $strNavQueryStringFull ?>"
		>
			1
		</a>
	<?php } ?>
	<?php // точки слева ?>
	<?php if ($arResult["nStartPage"] > 2) { ?>
		<a
			class="<?= $block->elem('page')->mod('dots') ?>"
			href="<?= $arResult["sUrlPath"] ?><?= $pageName ?><?= intVal(ceil($arResult["nStartPage"] / 2)) ?>/<?= $strNavQueryStringFull ?>"
		>
			...
		</a>
	<?php } ?>
	<?php // середка пагинации ?>
	<?php for ($i = $arResult["nStartPage"]; $i <= $arResult['nEndPage']; ++$i) { ?>
		<?php if ($i == $arResult["NavPageNomer"]) { ?>
			<span
				class="<?= $block->elem('page')->mod('current') ?>"
			>
				<?= $arResult["NavPageNomer"] ?>
			</span>
		<?php } else { ?>
			<a
				class="<?= $block->elem('page') ?>"
				href="<?= $arResult["sUrlPath"] ?><?= $i == 1 ? '' : $pageName.$i.'/' ?><?= $strNavQueryStringFull ?>"
			>
				<?= $i ?>
			</a>
		<?php } ?>
	<?php } ?>
	<?php // точки справа ?>
	<?php if ($arResult["nEndPage"] < $arResult['NavPageCount'] - 1) { ?>
		<a
			class="<?= $block->elem('page')->mod('dots') ?>"
			href="<?= $arResult["sUrlPath"] ?><?= $pageName ?><?= intVal(ceil(($arResult["NavPageCount"] - $arResult["nEndPage"]) / 2 + $arResult["nEndPage"])) ?>/<?= $strNavQueryStringFull ?>"
		>
			...
		</a>
	<?php } ?>
	<?php // последняя страница ?>
	<?php if ($arResult["nEndPage"] != $arResult['NavPageCount']) { ?>
		<a
			class="<?= $block->elem('page') ?>"
			href="<?= $arResult["sUrlPath"] ?><?= $pageName ?><?= $arResult['NavPageCount'] ?>/<?= $strNavQueryStringFull ?>"
		>
			<?= $arResult['NavPageCount'] ?>
		</a>
	<?php } ?>
	<?php // следующий ?>
	<?php if ($arResult['NavPageNomer'] != $arResult['NavPageCount']) { ?>
		<a
			class="<?= $block->elem('page')->mod('next') ?>"
			href="<?= $arResult["sUrlPath"] ?><?= $pageName ?><?= ($arResult["NavPageNomer"] + 1) ?>/<?= $strNavQueryStringFull ?>"
		>
			<?=GetMessage("nav_next")?>
		</a>
	<?php } ?>

	<?php // показать все ?>
	<?php if ($arResult["bShowAll"]) {
		if ($arResult["NavShowAll"]) { ?>
			<a
				class="<?= $block->elem('page')->mod('with-pages') ?>"
				href="<?= $arResult["sUrlPath"] ?><?= $strNavQueryStringFull ?>"
			>
				<?= GetMessage("nav_paged") ?>
			</a>
		<?php } else { ?>
			<a
				class="<?= $block->elem('page')->mod('all') ?>"
				href="<?= $arResult["sUrlPath"] ?><?= $pageName ?>all/<?= $strNavQueryStringFull ?>"
			>
				<?=GetMessage("nav_all")?>
			</a>
		<?php } ?>
	<?php } ?>
</div>
