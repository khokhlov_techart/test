@php
	$content = $content ?? '';
@endphp
<div class="{{ $block }}">
	{!! $content !!}
</div>