@php
	$logo = $logo ?? '';
@endphp

<div class="{{ $block }}">{!! $logo !!}</div>