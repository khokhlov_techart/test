import BEM from 'tao-bem';

class Header extends BEM.Block {
	static get blockName() {
		return 'b-header';
	}

	onInit() {
		console.log('Сработал onInit у блока header', this.el);
	}
}

Header.register();

export default Header;