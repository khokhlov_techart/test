@php
	$email = $email ?? '';
	$phone = $phone ?? '';
@endphp

<div class="{{ $block }}">
	<div class="{{ $block->elem('email') }}">{!! $email !!}</div>
	<div class="{{ $block->elem('phone') }}">{!! $phone !!}</div>
</div>