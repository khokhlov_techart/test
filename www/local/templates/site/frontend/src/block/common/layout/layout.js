import BEM from 'tao-bem';

class Layout extends BEM.Block {
	static get blockName() {
		return 'b-layout';
	}

	onInit() {
		console.log('Сработал onInit блока layout', this.el);
	}
}

Layout.register();

export default Layout;