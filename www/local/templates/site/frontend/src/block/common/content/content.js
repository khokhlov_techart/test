import BEM from 'tao-bem';

class Content extends BEM.Block {
	static get blockName() {
		return 'b-content';
	}

	onInit() {
		console.log('Сработал onInit у блока', this.el);
	}
}

Content.register();

export default Content;