import BEM from 'tao-bem';

class Footer extends BEM.Block {
	static get blockName() {
		return 'b-footer';
	}

	onInit() {
		console.log('Сработал onInit у блока footer', this.el);
	}
}

Footer.register();

export default Footer;