<?php if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>

	</main>
	<footer class="b-layout__footer">
		<?=\TAO::frontend()->renderBlock('common/footer', [
			'email' => 'info@techart.ru',
			'phone' => '+79999999999',
		]);?>
	</footer>
</body>
</html>
