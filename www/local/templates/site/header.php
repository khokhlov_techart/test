<?php if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>
<!DOCTYPE html>
<html lang="<?= LANGUAGE_ID ?>">
<head>
	<meta charset="UTF-8">
	<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" name="viewport">

	<title><?= $APPLICATION->ShowTitle()?></title>
	<?php $APPLICATION->ShowHead() ?>
	<?php \TAO::frontendCss('index') ?>
	<?php \TAO::frontendJs('index') ?>
</head>
<?php
//$APPLICATION->ShowPanel();
?> 
<body class="b-layout">
	<header class="b-layout__header">
		<?=\TAO::frontend()->renderBlock('common/header', [
			'logo' => 'techart',
		]);?>
	</header>
	<main class="b-layout__main">
