<?php
namespace App\CLI;

class Sitemap extends \TAO\CLI
{
	public function build_sitemap()
	{
		\TAO::sitemap()
			->protocol('http')
			->makeByAdminSettings(1)
			->finish();
		// В случае мультисайтовости раскомментировать и прописать id настроек остальных сайтов
		// \TAO::sitemap()->protocol('http')->makeByAdminSettings(2)->finish();
	}
}
