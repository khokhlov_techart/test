document.addEventListener('DOMContentLoaded', function() {
	var btnsOpen = document.querySelectorAll('.js-popup__btn');
	var btnsClose = document.querySelectorAll('.js-popup__btn-close');

	btnsOpen.forEach(function(btn) {
		btn.addEventListener(
			'click',
			function() {
				document.querySelector('.js-popup__form-' + btn.getAttribute('data-form-id')).classList.add('b-popup-form--open');
			},
			true
		);
	});

	btnsClose.forEach(function(btn) {
		btn.addEventListener(
			'click',
			function() {
				document.querySelector('.js-popup__form-' + btn.getAttribute('data-form-id')).classList.remove('b-popup-form--open');
			},
			true
		);
	});
});
