<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<?php
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
?>
<?php $id = uniqid() ?>
<button data-form-id="<?= $id ?>" class="b-popup-btn js-popup__btn"><?= $arParams['BUTTON_TEXT'] ?></button>
<div class="b-popup-form js-popup__form-<?= $id ?>">
	<div data-form-id="<?= $id ?>" class="b-popup__btn-close js-popup__btn-close">X</div>
	<?= \TAO::Form($arParams['FORM'])->setOption('layout', $arParams['TEMPLATE'])->setParentComponent($component)->render(); ?>
</div>
