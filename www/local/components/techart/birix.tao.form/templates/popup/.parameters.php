<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$arTemplateParameters = [
	'BUTTON_TEXT' => [
		'NAME' => 'Текст для кнопки',
		'TYPE' => 'STRING',
		"DEFAULT" => "",
	]
];
