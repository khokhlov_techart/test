<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

$arComponentDescription = [
	"NAME" => 'Форма bitrix.tao',
	"DESCRIPTION" => 'Подключение форм bitrix.tao',
	"ICON" => "",
	"SORT" => 10,
	"CACHE_PATH" => "Y",
	"PATH" => [
		"ID" => "techart",
		"NAME" => '',
		"CHILD" => [
			"ID" => "forms",
			"NAME" => 'Формы',
			"SORT" => 10,
		],
	],
];