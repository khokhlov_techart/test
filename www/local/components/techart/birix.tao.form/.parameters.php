<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

$forms = [];
$bundles = [];

foreach(\TAO::bundles() as $bundle) {
	$bundleForms = scandir($bundle->dir . '/lib/Form/');
	unset($bundleForms[0]);
	unset($bundleForms[1]);
	if ($bundleForms) {
		foreach ($bundleForms as $form) {
			$form = str_replace('.php', '', $form);
			$forms[$form] = $form;
			$bundles[$form] = $bundle;
		}
	}
}

$formClasses = scandir(\TAO::localDir() . '/forms/');
if (count($formClasses) > 2) {
	unset($formClasses[0]);
	unset($formClasses[1]);
	foreach ($formClasses as $form) {
		$form = str_replace('.php', '', $form);
		$forms[$form] = $form;
	}
}

$templates = [];

/**
 * Костыль(Public Морозов): класс что бы получить protected свойства, методы
 */
class FormForComponent extends \TAO\Form
{
	public function setName($name)
	{
		$this->name = $name;
	}

	public function templateDir()
	{
		return $this->fileDirs('views');
	}
}

$fFC = new FormForComponent();
$fFC->setName($arCurrentValues['FORM']);
$fFC->bundle = $bundles[$arCurrentValues['FORM']];

foreach($fFC->templateDir() as $dir) {
	$files = scandir($dir);
	foreach($files as $file) {
		if (strpos($file, 'layout-' ) !== false) {
			$nameLayout = str_replace(['layout-', '.phtml'], '', $file);
			$templates[$nameLayout] = $nameLayout;
		}
	}
}

$arComponentParameters = [
	'GROUPS' => array(),
	'PARAMETERS' => [
		"FORM" => [
			'PARENT' => 'BASE',
			'NAME' => 'Форма',
			'TYPE' => 'LIST',
			"VALUES" => $forms,
			"DEFAULT" => "",
			"REFRESH" => "Y",
		],
		"TEMPLATE" => [
			"PARENT" => "BASE",
			"NAME" => 'Шаблон',
			"TYPE" => "LIST",
			"VALUES" => $templates,
			"DEFAULT" => "",
		],
	]
];
