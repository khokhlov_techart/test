<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Highloadblock as HL;
use Bitrix\Main\Entity;

include_once('bannersTools.php');

class Banners extends \CBitrixComponent
{
	public function onPrepareComponentParams($params)
	{
		return $params;
	}

	public function executeComponent()
	{
		global $APPLICATION;


		$hlblock_id = $this->arParams['BLOCK_ID'];
		$hlblock = HL\HighloadBlockTable::getById($hlblock_id)->fetch();
		$entity = HL\HighloadBlockTable::compileEntity($hlblock);

		// uf info
		$fields = $GLOBALS['USER_FIELD_MANAGER']->GetUserFields('HLBLOCK_'.$hlblock['ID'], 0, LANGUAGE_ID);

		// sort

		$sortId = 'ID';
		$sortType = isset($this->arParams['SORT_ORDER']) ? $this->arParams['SORT_ORDER'] : 'DESC';
		if (isset($this->arParams['SORT_FIELD']) && isset($fields[$this->arParams['SORT_FIELD']]))
		{
			$sortId = $this->arParams['SORT_FIELD'];
		}

		$mainQuery = new Entity\Query($entity);
		$mainQuery->setFilter(
			array(
				[
					'LOGIC' => 'OR',
					[">=UF_EXPIRATION_DATE" => date('d.m.Y')],
					["=UF_EXPIRATION_DATE" => ""]
				],
				'<=UF_START_DATE' => date('d.m.Y'),
			)
		);
		$mainQuery->setSelect(array('*'));
		$mainQuery->setOrder(array($sortId => $sortType));


		$result = $mainQuery->exec();
		$result = new CDBResult($result);

		// build results
		$currentDir = $APPLICATION->GetCurDir();
		$items = array();
		while ($item = $result->fetch())
		{

			$rules = explode("\n", $item['UF_SHOW_ON_PAGES']);

			$isAllowed = true;

			foreach($rules as $rule) {
				if (preg_match('{^(Disallow):\s+(.+)}', $rule, $matches) !== 0) {
					if(preg_match('{^' .rtrim($matches[2]) . '}', $currentDir)) {
						$isAllowed = false;
					}
				} else {
					if (preg_match('{^(Allow):\s+(.+)}', $rule, $matches) == 0) {
						$isAllowed = true;
					} else {
						if ($currentDir == '/') {
							$isAllowed = false;
						}
					}
				}

				if (preg_match('{^(Allow):\s+(.+)}', $rule, $matches) !== 0) {
					if(preg_match('{^'.rtrim($matches[2]).'}', $currentDir)) {
						$isAllowed = true;
					}
				}
			}


			$textPlace = BannersTools::getListValue($item["UF_TEXT_PLACE"]);
			$isActive = BannersTools::getListValue($item["UF_ACTIVE"]);

			$item['UF_IMAGE_DESKTOP'] = BannersTools::getImage($item['UF_IMAGE_DESKTOP']);
			$item['UF_IMAGE_MOBILE'] = BannersTools::getImage($item['UF_IMAGE_MOBILE']);
			$item['UF_IMAGE_BACKGROUND'] = BannersTools::getImage($item['UF_IMAGE_BACKGROUND']);
			$item['UF_TEXT_PLACE'] = $textPlace;

			if ($isActive === 'Y' & $isAllowed === true) {
				$items[$item['ID']] = $item;
				$ids[] = $item['ID'];
			}
		}

		if ($ids) {
			$bannerToShow = BannersTools::getRandomBanner($ids);

			$this->arResult['item'] = $items[$bannerToShow];
			$this->arResult['fields'] = $fields;
			$this->arResult['sort_id'] = $sortId;
			$this->arResult['sort_type'] = $sortType;

			$urlForAddNew = '/bitrix/admin/highloadblock_row_edit.php?ENTITY_ID=' . $this->arParams['BLOCK_ID'] . '&lang=ru&bxpublic=Y';

			$link = $APPLICATION->GetPopupLink([
				'URL' => $urlForAddNew
			]);

			$buttonForAddNew = [
				"URL" => 'javascript:'.$link,
				'TEXT' => 'Добавить элемент',
				'TITLE' => 'Добавить элемент',
				'ICON' => 'bx-context-toolbar-create-icon',
				"ID" => "bx-context-toolbar-add-element",
			];

			$this->addIncludeAreaIcon(
				$buttonForAddNew
			);

			$this->includeComponentTemplate();
		}
	}
}
