<?php if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true)die();

use Bitrix\Highloadblock as HL;

$hlBlocks = array();

$hlObjects = HL\HighloadBlockTable::getList();

// var_dump($HLs->Fetch());
while ($hlBlock = $hlObjects->Fetch()) {
	$hlBlocks[$hlBlock['ID']] = "[" . $hlBlock["ID"] . "] " . $hlBlock["NAME"];;
}

$arComponentParameters = array(
	'GROUPS' => array(
	),
	'PARAMETERS' => array(
		'BLOCK_ID' => array(
			'PARENT' => 'BASE',
			'NAME' => GetMessage('HLLIST_COMPONENT_BLOCK_ID_PARAM'),
			"TYPE" => "LIST",
			"VALUES" => $hlBlocks,
			"DEFAULT" => '={$_REQUEST["ID"]}',
			"ADDITIONAL_VALUES" => "Y",
			"REFRESH" => "Y",
			// 'DEFAULT' => 'advertising_banners',
			// 'TYPE' => 'TEXT'
		),
	),
);