<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

class BannersTools
{
	public static function getFile($id)
	{
		if ($id) {
			return CFile::GetFileArray($id);
		}
		return null;
	}

	public static function getImage($id)
	{
		if ($id) {
			return CFile::ResizeImageGet($id, ['width' => 1920, 'height' => 9999], BX_RESIZE_IMAGE_PROPORTIONAL);
		}
		return null;
	}

	public static function getListValue($userFieldId) {
		if ($userFieldId) {
			$arListProp  = CUserFieldEnum::GetList([], ["ID" => $userFieldId]);

			if($listProp = $arListProp->GetNext()) {
				return $listProp['XML_ID'];
			}
		}
		return null;
	}

	public function getRandomBanner($bannersIds)
	{
		if (sizeof($bannersIds) == 0)
			return false;
		return $bannersIds[rand(0, sizeof($bannersIds) - 1)];
	}
}
?>
