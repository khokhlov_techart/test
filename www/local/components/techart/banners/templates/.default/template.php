<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) { die(); }
$this->AddEditAction(
	$arResult['item']['ID'],
	"/bitrix/admin/highloadblock_row_edit.php?ENTITY_ID=" . $arParams['BLOCK_ID'] . "&ID=" . $arResult['item']['ID'] . "&lang=ru&bxpublic=Y",
	"Изменить " . $arResult['item']['ID'],
	Array(
		"WINDOW" => array("wight"=>780, "height"=>500),
		"ICON" => "bx-context-toolbar-edit-icon",
		"SRC" => "/bitrix/images/myicon.gif"
	)
);
?>

<section>
	<article id="<?= $this->GetEditAreaId($arResult['item']['ID']); ?>" class="b-image-block" style="background-image: url(<?=$arResult['item']['UF_IMAGE_BACKGROUND']['src']?>);">
	<?php if ($arResult['item']['UF_LINK']) {?>
		<a href="<?=$arResult['item']['UF_LINK']?>" target="_blank" class="b-image-block__link">
	<?php } ?>
		<div class="b-image-block__wrapper b-image-block__wrapper--<?=$arResult['item']['UF_TEXT_PLACE']?>">
			<div class="b-image-block__image-wrapper">
				<picture>
					<source srcset="<?=$arResult['item']['UF_IMAGE_MOBILE']['src']?>" media="(max-width: 959px)">
					<source srcset="<?=$arResult['item']['UF_IMAGE_DESKTOP']['src']?>" media="(min-width: 960px)">
					<img class="b-image-block__image" src="<?=$arResult['item']['UF_IMAGE_DESKTOP']['src']?>" alt="<?=$arResult['item']['UF_TITLE']?>">
				</picture>
			</div>
			<div class="b-image-block__content" style="color: <?=$arResult['item']['UF_TEXT_COLOR']?>">
				<div class="b-image-block__title"><?=$arResult['item']['UF_TITLE']?></div>
				<div class="b-image-block__text"><?=$arResult['item']['UF_TEXT']?></div>
			</div>
		</div>
	<?php if ($arResult['item']['UF_LINK']) {?>
		</a>
	<?php }?>
	</article>
</section>
