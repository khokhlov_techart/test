<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

$arComponentDescription = [
	"NAME" => GetMessage("COMPONENT_NAME"),
	"DESCRIPTION" => GetMessage("COMPONENT_DESC"),
	"ICON" => "",
	"SORT" => 10,
	"CACHE_PATH" => "Y",
	"PATH" => [
		"ID" => "techart",
		"NAME" => GetMessage("ROOT_DIR_NAME"),
		"CHILD" => [
			"ID" => "elements",
			"NAME" => GetMessage("CHILD_NAME"),
			"SORT" => 10,
		],
	],
];