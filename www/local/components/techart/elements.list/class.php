<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Loader;
use Bitrix\Iblock;

/**
 * Class ElementsList
 */
class ElementsList extends \CBitrixComponent
{
	private $arrFilter = array();
	private $arSelect = array(
		"ID",
		"IBLOCK_ID",
		"IBLOCK_SECTION_ID",
		"NAME",
		"ACTIVE_FROM",
		"TIMESTAMP_X",
		"DETAIL_PAGE_URL",
		"LIST_PAGE_URL",
		"DETAIL_TEXT",
		"DETAIL_TEXT_TYPE",
		"PREVIEW_TEXT",
		"PREVIEW_TEXT_TYPE",
		"PREVIEW_PICTURE",
	);

	private $arNavParams = false;
	private $arNavigation = false;
	private $parentSection = 0;

	public function onPrepareComponentParams($arParams)
	{
		global $DB;

		$result = $arParams;

		$result["DETAIL_URL"] = trim($arParams["DETAIL_URL"]);
		$result['IBLOCK_TYPE'] = trim($arParams['IBLOCK_TYPE']);
		$result["IBLOCK_ID"] = trim($arParams["IBLOCK_ID"]);
		$result['IBLOCK_CODE'] = trim($arParams["IBLOCK_CODE"]);
		$result["CHECK_DATES"] = $arParams["CHECK_DATES"] != "N";
		$result["STRICT_SECTION_CHECK"] = (isset($arParams["STRICT_SECTION_CHECK"]) && $arParams["STRICT_SECTION_CHECK"] === "Y");
		$result["PARENT_SECTION"] = intval($arParams["PARENT_SECTION"]);
		$result["PARENT_SECTION_CODE"] = trim($arParams["PARENT_SECTION_CODE"]);
		$result["ELEMENTS_COUNT"] = intval($arParams["ELEMENTS_COUNT"]);

		if ((empty($result['IBLOCK_ID']) || $result['IBLOCK_ID'] === '0') && !empty($result['IBLOCK_CODE'])) {
			$iblockType = '';
			if (!empty($result['IBLOCK_TYPE']) && $result['IBLOCK_TYPE'] !== '-') {
				$iblockType = $result['IBLOCK_TYPE'];
			}
			$ib_list = CIBlock::GetList(
				Array(),
				Array(
					'TYPE' => $iblockType,
					"CODE" => $result['IBLOCK_CODE'],
					"CHECK_PERMISSIONS" => "N"
				)
			);
			if ($idListRes = $ib_list->Fetch()) {
				$result["IBLOCK_ID"] = $idListRes['ID'];
			}
		}

		if ($result["ELEMENTS_COUNT"] <= 0) {
			$result["ELEMENTS_COUNT"] = 20;
		}

		$result["ACTIVE_DATE_FORMAT"] = trim($arParams["ACTIVE_DATE_FORMAT"]);

		if (strlen($result["ACTIVE_DATE_FORMAT"]) <= 0) {
			$result["ACTIVE_DATE_FORMAT"] = $DB->DateFormatToPHP(CSite::GetDateFormat("SHORT"));
		}

		$result["CHECK_PERMISSIONS"] = $arParams["CHECK_PERMISSIONS"] != "N";

		if (strlen($arParams["FILTER_NAME"]) <= 0 || !preg_match("/^[A-Za-z_][A-Za-z01-9_]*$/", $arParams["FILTER_NAME"])) {
			$this->arrFilter = array();
		} else {
			$this->arrFilter = $GLOBALS[$arParams["FILTER_NAME"]];

			if (!is_array($this->arrFilter)) {
				$this->arrFilter = array();
			}
		}

		// Пользовательские свойства
		$result["PROPERTY_CODE"] = is_array($arParams["PROPERTY_CODE"]) ? $arParams["PROPERTY_CODE"] : array();

		foreach ($result["PROPERTY_CODE"] as $key => $val) {
			if ($val === "") {
				unset($result["PROPERTY_CODE"][$key]);
			}
		}

		// Параметры сортировки
		$result["SORT_BY1"] = trim($arParams["SORT_BY1"]);

		if (strlen($result["SORT_BY1"]) <= 0) {
			$result["SORT_BY1"] = "ACTIVE_FROM";
		}

		if (!preg_match('/^(asc|desc|nulls)(,asc|,desc|,nulls){0,1}$/i', $arParams["SORT_ORDER1"])) {
			$result["SORT_ORDER1"] = "DESC";
		} else {
			$result['SORT_ORDER1'] = $arParams['SORT_ORDER1'];
		}

		$result['SORT_ORDER2'] = trim($arParams['SORT_ORDER2']);

		if (strlen($arParams["SORT_BY2"]) <= 0) {
			if (strtoupper($result["SORT_BY1"]) == 'SORT') {
				$result["SORT_BY2"] = "ID";
				$result["SORT_ORDER2"] = "DESC";
			} else {
				$result["SORT_BY2"] = "SORT";
			}
		} else {
			$result['SORT_BY2'] = trim($arParams['SORT_BY2']);
		}

		if (!preg_match('/^(asc|desc|nulls)(,asc|,desc|,nulls){0,1}$/i', $result["SORT_ORDER2"])) {
			$result["SORT_ORDER2"] = "ASC";
		}

		// Параметры кеширования
		$result['CACHE_TYPE'] = $arParams['CACHE_TYPE'];
		$result["CACHE_TIME"] = isset($arParams["CACHE_TIME"]) ? $arParams["CACHE_TIME"] : 36000000;

		if (!$arParams["CACHE_FILTER"] && count($this->arrFilter) > 0) {
			$result["CACHE_TIME"] = 0;
		}

		$result["CACHE_GROUPS"] = $arParams["CACHE_GROUPS"] != 'N';

		// Параметры постраничной навигации
		$result['PAGER_TEMPLATE'] = $arParams['PAGER_TEMPLATE'];
		$result['PAGER_TITLE'] = $arParams['PAGER_TITLE'];
		$result['DISPLAY_TOP_PAGER'] = $arParams['DISPLAY_TOP_PAGER'] == 'Y';
		$result['DISPLAY_BOTTOM_PAGER'] = $arParams['DISPLAY_BOTTOM_PAGER'] != 'N';
		$result['PAGER_SHOW_ALWAYS'] = $arParams['PAGER_SHOW_ALWAYS'] == 'Y';
		$result['PAGER_SHOW_ALL'] = $arParams['PAGER_SHOW_ALL'] == 'Y';

		return $result;
	}

	public function executeComponent()
	{
		CPageOption::SetOptionString('main', 'nav_page_in_session', 'N');

		$this->setNavParams();

		if (!$this->readDataFromCache()) {
			if (!Loader::includeModule("iblock")) {
				$this->abortResultCache();

				\ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));

				return;
			}

			$this->setParentSection();

			if ($this->arParams["STRICT_SECTION_CHECK"] && ($this->arParams["PARENT_SECTION"] > 0 || strlen($this->arParams["PARENT_SECTION_CODE"]) > 0)) {
				if ($this->parentSection <= 0) {
					$this->abortResultCache();

					Iblock\Component\Tools::process404(
						trim($this->arParams["MESSAGE_404"]) ?: GetMessage("T_NEWS_NEWS_NA"),
						true,
						$this->arParams["SET_STATUS_404"] === "Y",
						$this->arParams["SHOW_404"] === "Y",
						$this->arParams["FILE_404"]
					);
					return;
				}
			}

			$rsElement = $this->getElementsObj();
			$this->arResult['ITEMS'] = $this->getItemsArray($rsElement);
			$this->arResult['NAV_STRING'] = $this->getNavString($rsElement);

			$this->includeComponentTemplate();
		}
	}

	/**
	 * Возвращает результирующий объект с выборкой элементов
	 *
	 * @return mixed
	 */
	private function getElementsObj()
	{
		$arSort = [
			$this->arParams["SORT_BY1"] => $this->arParams["SORT_ORDER1"],
			$this->arParams["SORT_BY2"] => $this->arParams["SORT_ORDER2"],
		];

		if (!array_key_exists("ID", $arSort)) {
			$arSort["ID"] = "DESC";
		}

		// Формирование фильтра
		$arFilter = [
			'IBLOCK_ID' => $this->arParams['IBLOCK_ID'],
			'IBLOCK_LID' => SITE_ID,
			'ACTIVE' => 'Y',
			'CHECK_PERMISSIONS' => $this->arParams['CHECK_PERMISSIONS'] ? 'Y' : 'N',
		];

		if ($this->arParams["CHECK_DATES"]) {
			$arFilter["ACTIVE_DATE"] = "Y";
		}

		if ($this->parentSection > 0) {
			$arFilter['SECTION_ID'] = $this->parentSection;
		}

		$rsElement = CIBlockElement::GetList($arSort, array_merge($arFilter, $this->arrFilter), false, $this->arNavParams, $this->arSelect);
		$rsElement->SetUrlTemplates($this->arParams["DETAIL_URL"], "", $this->arParams["IBLOCK_URL"]);

		return $rsElement;
	}

	/**
	 * Возвращает сформированный массив с элементами
	 *
	 * @param CIBLockResult $rsElement - выборка, на основе которой формируется список элементов
	 * @return array
	 */
	private function getItemsArray($rsElement)
	{
		$arItems = array();
		$bGetProperty = count($this->arParams["PROPERTY_CODE"]) > 0;

		if ($bGetProperty) {
			$arSelect[] = "PROPERTY_*";
		}

		while ($obElement = $rsElement->GetNextElement()) {
			$arItem = $obElement->GetFields();

			// Формирование ссылок редактирования элемента в визуальном редакторе
			$arButtons = CIBlock::GetPanelButtons(
				$arItem["IBLOCK_ID"],
				$arItem["ID"],
				0,
				["SECTION_BUTTONS" => false, "SESSID" => false]
			);

			$arItem["EDIT_LINK"] = $arButtons["edit"]["edit_element"]["ACTION_URL"];
			$arItem["DELETE_LINK"] = $arButtons["edit"]["delete_element"]["ACTION_URL"];

			// Получение свойств (если запрошено хотя бы одно существующее)
			if ($bGetProperty) {
				$arItem["PROPERTIES"] = $obElement->GetProperties();

				$arItem["DISPLAY_PROPERTIES"] = array();

				foreach ($this->arParams["PROPERTY_CODE"] as $pid) {
					$prop = &$arItem["PROPERTIES"][$pid];

					if (is_array($prop["VALUE"]) && count($prop["VALUE"]) > 0 || !is_array($prop["VALUE"]) && strlen($prop["VALUE"]) > 0) {
						$arItem["DISPLAY_PROPERTIES"][$pid] = CIBlockFormatProperties::GetDisplayValue($arItem, $prop, "news_out");
					}
				}
			}

			// Формирование массива данных изображения
			Iblock\Component\Tools::getFieldImageData(
				$arItem,
				array('PREVIEW_PICTURE', 'DETAIL_PICTURE'),
				Iblock\Component\Tools::IPROPERTY_ENTITY_ELEMENT,
				'IPROPERTY_VALUES'
			);

			if (strlen($arItem["ACTIVE_FROM"]) > 0) {
				$arItem["DISPLAY_ACTIVE_FROM"] = CIBlockFormatProperties::DateFormat($this->arParams["ACTIVE_DATE_FORMAT"], MakeTimeStamp($arItem["ACTIVE_FROM"], CSite::GetDateFormat()));
			} else {
				$arItem["DISPLAY_ACTIVE_FROM"] = "";
			}

			$arItems[] = $arItem;
		}

		return $arItems;
	}

	/**
	 * Возвращает панель постраничной навигации в HTML виде
	 *
	 * @param CIBLockResult $rsElement - выборка, на основе которой формируется навигация
	 * @return mixed
	 */
	private function getNavString($rsElement)
	{
		global $navComponentObject;

		$navComponentParameters = array();

		$res = $rsElement->GetPageNavStringEx(
			$navComponentObject,
			$this->arParams['PAGER_TITLE'],
			$this->arParams['PAGER_TEMPLATE'],
			$this->arParams['PAGER_SHOW_ALWAYS'],
			$this,
			$navComponentParameters
		);

		return $res;
	}

	/**
	 * Формирует параметров навигации
	 */
	private function setNavParams()
	{
		if ($this->arParams['DISPLAY_TOP_PAGER'] || $this->arParams['DISPLAY_BOTTOM_PAGER']) {
			$arNavParams = array(
				'nPageSize' => $this->arParams['ELEMENTS_COUNT'],
				'bShowAll' => $this->arParams['PAGER_SHOW_ALL'],
			);
			$arNavigation = CDBResult::GetNavParams($arNavParams);
		} else {
			$arNavParams = array(
				'nTopCount' => $this->arParams['ELEMENTS_COUNT'],
			);
			$arNavigation = false;
		}

		$this->arNavParams = $arNavParams;
		$this->arNavigation = $arNavigation;
	}

	/**
	 *  Устанавливает родительский раздел
	 */
	private function setParentSection()
	{
		$this->parentSection = CIBlockFindTools::GetSectionID(
			$this->arParams["PARENT_SECTION"],
			$this->arParams["PARENT_SECTION_CODE"],
			array(
				"GLOBAL_ACTIVE" => "Y",
				"IBLOCK_ID" => $this->arParams["IBLOCK_ID"],
			)
		);
	}

	/**
	 * Определяет читать данные из кеша или нет
	 *
	 * @return bool <b>true</b> - если считывание происходит из кеша, иначе <b>false</b>
	 */
	protected function readDataFromCache()
	{
		global $USER;

		$additionalCacheID = [
			$this->arParams['CACHE_GROUPS'] ? $USER->GetGroups() : false,
			$this->arrFilter,
			$this->arNavigation,
		];

		return !$this->startResultCache(false, $additionalCacheID);
	}
}
