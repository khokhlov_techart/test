<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<?php
/**
 * @var array $arParams
 * @var array $arResult
 */
?>
<div class="b-elements-list">
	<?php if ($arParams['DISPLAY_TOP_PAGER']) {
		echo $arResult['NAV_STRING'];
	} ?>
	<section class="b-elements-list__items">
		<?php foreach ($arResult["ITEMS"] as $arItem) { ?>
			<?php
			$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
			$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
			?>
			<article class="b-elements-list-item b-elements-list__item" id="<?= $this->GetEditAreaId($arItem['ID']); ?>">
				<div class="b-elements-list-item__img-container">
					<?php if ($arItem['PREVIEW_PICTURE']) { ?>
						<img
							class="b-elements-list-item__img"
							src="<?= $arItem['RESIZED_PREVIEW_PICTURE']['src'] ?>"
							alt="<?= $arItem['PREVIEW_PICTURE']['ALT'] ?>"
							title="<?= $arItem['PREVIEW_PICTURE']['TITLE'] ?>"
							width="<?= $arItem['RESIZED_PREVIEW_PICTURE']['width'] ?>"
							height="<?= $arItem['RESIZED_PREVIEW_PICTURE']['height'] ?>"
						>
					<?php } ?>
				</div>
				<div class="b-elements-list-item__right-box">
					<div class="b-elements-list-item__name">
						<span class="b-elements-list-item__id">#<?= $arItem['ID'] ?></span>
						<a class="b-elements-list-item__link"
						   href="<?= $arItem['DETAIL_PAGE_URL'] ?>"><?= $arItem['NAME'] ?></a>
					</div>
					<div class="b-elements-list-item__text"><?= $arItem['PREVIEW_TEXT'] ?></div>
					<?php if ($arItem['ACTIVE_FROM']) { ?>
						<div class="b-elements-list-item__date"><?= $arItem['DISPLAY_ACTIVE_FROM'] ?></div>
					<?php } ?>
				</div>
			</article>
		<?php } ?>
	</section>
	<?php
	if ($arParams['DISPLAY_BOTTOM_PAGER']) {
		echo $arResult['NAV_STRING'];
	}
	?>
</div>
