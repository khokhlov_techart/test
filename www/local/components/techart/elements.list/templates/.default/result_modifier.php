<?php
foreach ($arResult["ITEMS"] as &$arItem) {
	if ($arItem['PREVIEW_PICTURE']) {
		$arItem['RESIZED_PREVIEW_PICTURE'] = CFile::ResizeImageGet(
			$arItem['PREVIEW_PICTURE'],
			array('width' => 150, 'height' => 150),
			BX_RESIZE_IMAGE_PROPORTIONAL,
			true
		);
	}
}

unset($arItem);
