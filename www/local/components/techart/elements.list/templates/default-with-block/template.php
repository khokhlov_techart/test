<?php
$block = $arParams['USE_BEM_BLOCK'] ?: 'blank/elements-list';
$renderedElements = [];
foreach ($arResult["PREPARED_ITEMS"] as $arItem) {
	$renderedElements[] = \TAO::frontend()->renderBlock($block . '-item', $arItem);
}
?>

<?= \TAO::frontend()->renderBlock($block, ['items' => $renderedElements]) ?>
