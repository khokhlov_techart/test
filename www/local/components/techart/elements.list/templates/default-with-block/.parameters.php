<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

$arTemplateParameters = array(
	'USE_BEM_BLOCK' => array(
		'PARENT' => 'COMPONENT_TEMPLATE',
		'NAME' => 'Блок для отображения',
		'TYPE' => 'LIST',
		'VALUES' => [
			'blank/elements-list' => 'Лентой на всю ширину',
			'blank/tile-elements-list' => '3 столбца',
		],
		'REFRESH' => 'Y',
	),
);
