<?php
foreach ($arResult["ITEMS"] as $arItem) {
	// проводим "всякие махинации" для редактирования элементов в режиме правки
	$this->AddEditAction(
		$arItem['ID'],
		$arItem['EDIT_LINK'],
		CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT")
	);
	$this->AddDeleteAction(
		$arItem['ID'],
		$arItem['DELETE_LINK'],
		CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"),
		array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM'))
	);

	$image = '';
	if ($arItem['PREVIEW_PICTURE']) {
		$resizedImage = CFile::ResizeImageGet(
			$arItem['PREVIEW_PICTURE'],
			array('width' => 150, 'height' => 150),
			BX_RESIZE_IMAGE_PROPORTIONAL,
			true
		);
		$image = [
			'src' => $resizedImage['src'],
			'width' => $resizedImage['width'],
			'height' => $resizedImage['height'],
			'alt' => $arItem['PREVIEW_PICTURE']['ALT'],
			'title' => $arItem['PREVIEW_PICTURE']['TITLE'],
		];
	}

	$arResult['PREPARED_ITEMS'][] = [
		'id' => $arItem['ID'],
		'date' => $arItem['DISPLAY_ACTIVE_FROM'],
		'title' => $arItem['NAME'],
		'text' => $arItem['PREVIEW_TEXT'],
		'url' => $arItem['DETAIL_PAGE_URL'],
		'image' => $image,
		// важно высчитывать этот id в том же месте, где проводили "всякие махинации" (см. выше)
		'editAreaId' => $this->GetEditAreaId($arItem['ID']),
	];
}
