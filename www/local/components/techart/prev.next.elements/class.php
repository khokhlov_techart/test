<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

class PrevNextElements extends \CBitrixComponent
{
	public function onPrepareComponentParams($arParams)
	{
		return $arParams;
	}

	public function executeComponent()
	{
		$this->getNavLinks();
		$this->includeComponentTemplate();
	}

	protected function getNavLinks()
	{
		$filter = [
			 'IBLOCK_ID' => $this->arParams["IBLOCK_ID"],
			 'ACTIVE_DATE' => 'Y',
			 'ACTIVE' => 'Y',
			 'IBLOCK_SECTION_ID' => $this->arParams['IBLOCK_SECTION_ID']
		];

		if (is_array($this->arParams['ADDITION_FILTER'])) {
			$filter = array_merge($filter, $this->arParams['ADDITION_FILTER']);
		}

		$res = CIBlockElement::GetList(
			[
				$this->arParams['SORT_BY1'] => $this->arParams['SORT_ORDER1'],
				$this->arParams['SORT_BY2'] => $this->arParams['SORT_ORDER2'],
			],
			$filter,
			false,
			['nPageSize' => '1', 'nElementID' => $this->arParams['ELEMENT_ID']],
			[]
		);

		$elements = [];
		while ($ob = $res->GetNext()){
			$elements[] = $ob;
		}

		if (count($elements) == 3) {
			$this->arResult['PREVIEW_LINK'] = $elements[2];
			$this->arResult['NEXT_LINK'] = $elements[0];
			return;
		}

		if (count($elements) == 2) {
			if ($elements[0]['ID'] == $this->arParams['ELEMENT_ID']) {
				$this->arResult['PREVIEW_LINK'] = $elements[1];
			} else {
				$this->arResult['NEXT_LINK'] = $elements[0];
			}
			return;
		}
	}
}
