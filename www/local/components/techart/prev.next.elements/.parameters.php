<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arCurrentValues */

if (!CModule::IncludeModule("iblock")) {
	return;
}

// Получение инфоблока
$arTypesEx = CIBlockParameters::GetIBlockTypes(["-" => " "]);

$arIBlocks = array();
$objIBlock = CIBlock::GetList(["SORT" => "ASC"], ["SITE_ID" => $_REQUEST["site"], "TYPE" => ($arCurrentValues["IBLOCK_TYPE"] != "-" ? $arCurrentValues["IBLOCK_TYPE"] : "")]);

while ($arRes = $objIBlock->Fetch()) {
	$arIBlocks[$arRes["ID"]] = "[" . $arRes["ID"] . "] " . $arRes["NAME"];
}

// Получение возможных сортировок
$arSorts = ["ASC" => "По возрастанию", "DESC" => "По убыванию"];
$arSortFields = [
	"ID" => "ID",
	"NAME" => "Имя",
	"ACTIVE_FROM" => "Дата активности",
	"SORT" => "Поле сортировки",
	"TIMESTAMP_X" => "Дата изменения",
];

// Получение ствойств
$arProperty_LNS = array();
$rsProp = CIBlockProperty::GetList(["sort" => "asc", "name" => "asc"], ["ACTIVE" => "Y", "IBLOCK_ID" => (isset($arCurrentValues["IBLOCK_ID"]) ? $arCurrentValues["IBLOCK_ID"] : $arCurrentValues["ID"])]);

while ($arr = $rsProp->Fetch()) {
	$arProperty[$arr["CODE"]] = "[" . $arr["CODE"] . "] " . $arr["NAME"];

	if (in_array($arr["PROPERTY_TYPE"], ["L", "N", "S"])) {
		$arProperty_LNS[$arr["CODE"]] = "[" . $arr["CODE"] . "] " . $arr["NAME"];
	}
}

$arComponentParameters = [
	'GROUPS' => array(),
	'PARAMETERS' => [
		"IBLOCK_TYPE" => [
			'PARENT' => 'BASE',
			'NAME' => 'Тип инфоблока',
			'TYPE' => 'LIST',
			"VALUES" => $arTypesEx,
			"DEFAULT" => "news",
			"REFRESH" => "Y",
		],
		"IBLOCK_ID" => [
			"PARENT" => "BASE",
			"NAME" => 'ID инфоблока',
			"TYPE" => "LIST",
			"VALUES" => $arIBlocks,
			"DEFAULT" => '={$_REQUEST["ID"]}',
			"ADDITIONAL_VALUES" => "Y",
			"REFRESH" => "Y",
		],
		"ELEMENTS_ID" => [
			"PARENT" => "BASE",
			"NAME" => 'ID элемента',
			"TYPE" => "STRING",
			"DEFAULT" => "20",
		],
		"SORT_BY1" => [
			"PARENT" => "DATA_SOURCE",
			"NAME" => 'Поле сортировки 1',
			"TYPE" => "LIST",
			"DEFAULT" => "ACTIVE_FROM",
			"VALUES" => $arSortFields,
			"ADDITIONAL_VALUES" => "Y",
		],
		"SORT_ORDER1" => [
			"PARENT" => "DATA_SOURCE",
			"NAME" => 'Направление сортировки 1',
			"TYPE" => "LIST",
			"DEFAULT" => "DESC",
			"VALUES" => $arSorts,
			"ADDITIONAL_VALUES" => "Y",
		],
		"SORT_BY2" => [
			"PARENT" => "DATA_SOURCE",
			"NAME" => 'Поле сортировки 2',
			"TYPE" => "LIST",
			"DEFAULT" => "SORT",
			"VALUES" => $arSortFields,
			"ADDITIONAL_VALUES" => "Y",
		],
		"SORT_ORDER2" => [
			"PARENT" => "DATA_SOURCE",
			"NAME" => 'Направление сортировки 2',
			"TYPE" => "LIST",
			"DEFAULT" => "SORT",
			"VALUES" => $arSorts,
			"ADDITIONAL_VALUES" => "Y",
		],
		"ADDITION_FILTER" => [
			"PARENT" => "DATA_SOURCE",
			"NAME" => 'Дополнительный фильтр(Массив)',
			"TYPE" => "STRING",
		],
	],
];

