<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

$arComponentDescription = [
	"NAME" => "Предыдущая/следующая запись",
	"DESCRIPTION" => "Предыдущая/следующая запись",
	"ICON" => "",
	"SORT" => 10,
	"CACHE_PATH" => "Y",
	"PATH" => [
		"ID" => "techart",
		"NAME" => 'Techart',
		"CHILD" => [
			"ID" => "elements",
			"NAME" => 'Инфоблоки',
			"SORT" => 10,
		],
	],
];