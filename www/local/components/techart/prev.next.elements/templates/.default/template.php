<nav>
	<?php if ($arResult['PREVIEW_LINK']) { ?>
		<a href="<?= $arResult['PREVIEW_LINK']['DETAIL_PAGE_URL'] ?>">Предыдущая</a>
	<?php } ?>
	<?php if ($arResult['NEXT_LINK']) { ?>
		<a href="<?= $arResult['NEXT_LINK']['DETAIL_PAGE_URL'] ?>">Следующая</a>
	<?php } ?>
</nav>
