<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Highloadblock as HL;
use Bitrix\Main\Entity;

include_once('highloadblockListTools.php');

class HighloadBlockList extends \CBitrixComponent
{
	public function onPrepareComponentParams($params)
	{
		return $params;
	}

	public function executeComponent()
	{
		global $APPLICATION;

		$hlblock_id = $this->arParams['BLOCK_ID'];
		$hlblock = HL\HighloadBlockTable::getById($hlblock_id)->fetch();
		$entity = HL\HighloadBlockTable::compileEntity($hlblock);

		// uf info
		$fields = $GLOBALS['USER_FIELD_MANAGER']->GetUserFields('HLBLOCK_'.$hlblock['ID'], 0, LANGUAGE_ID);


		// sort
		$sortId = 'ID';
		$sortType = isset($this->arParams['SORT_ORDER']) ? $this->arParams['SORT_ORDER'] : 'DESC';
		if (isset($this->arParams['SORT_FIELD']) && isset($fields[$this->arParams['SORT_FIELD']]))
		{
			$sortId = $this->arParams['SORT_FIELD'];
		}

		// pagen
		if (isset($this->arParams['ROWS_PER_PAGE']) && $this->arParams['ROWS_PER_PAGE']>0) {
			$pagenId = isset($this->arParams['PAGEN_ID']) && trim($this->arParams['PAGEN_ID']) != '' ? trim($this->arParams['PAGEN_ID']) : 'page';
			$perPage = intval($this->arParams['ROWS_PER_PAGE']);
			$nav = new \Bitrix\Main\UI\PageNavigation($pagenId);
			$nav->allowAllRecords(true)
				->setPageSize($perPage)
				->initFromUri();
		} else {
			$this->arParams['ROWS_PER_PAGE'] = 0;
		}


		// start query
		$mainQuery = new Entity\Query($entity);
		$mainQuery->setSelect(array('*'));
		$mainQuery->setOrder(array($sortId => $sortType));

		// filter
		if (
			isset($this->arParams['FILTER_NAME']) &&
			!empty($this->arParams['FILTER_NAME']) &&
			preg_match('/^[A-Za-z_][A-Za-z01-9_]*$/', $this->arParams['FILTER_NAME']))
		{
			global ${$this->arParams['FILTER_NAME']};
			$filter = ${$this->arParams['FILTER_NAME']};
			if (is_array($filter))
			{
				$mainQuery->setFilter($filter);
			}
		}

		// pagen
		if ($perPage > 0)
		{
			$mainQueryCnt = $mainQuery;
			$result = $mainQueryCnt->exec();
			$result = new \CDBResult($result);
			$nav->setRecordCount($result->selectedRowsCount());
			$this->arResult['nav_object'] = $nav;
			unset($mainQueryCnt, $result);

			$mainQuery->setLimit($nav->getLimit());
			$mainQuery->setOffset($nav->getOffset());
		}

		$result = $mainQuery->exec();
		$result = new CDBResult($result);

		// build results
		$rows = array();
		$tableColumns = array();
		while ($row = $result->fetch())
		{
			foreach ($row as $k => $v)
			{
				$arUserField = $fields[$k];

				if ($k == 'ID')
				{
					$tableColumns['ID'] = true;
					continue;
				}
				if ($arUserField['SHOW_IN_LIST'] != 'Y')
				{
					continue;
				}

				$tableColumns[$k] = true;
				$row[$k] = CHighloadblockTools::getFieldData($arUserField['USER_TYPE_ID'], $v);;
			}

			$rows[] = $row;
		}

		$this->arResult['rows'] = $rows;
		$this->arResult['fields'] = $fields;
		$this->arResult['tableColumns'] = $tableColumns;
		$this->arResult['sort_id'] = $sortId;
		$this->arResult['sort_type'] = $sortType;

		$urlForAddNew = '/bitrix/admin/highloadblock_row_edit.php?ENTITY_ID=' . $this->arParams['BLOCK_ID'] . '&lang=ru&bxpublic=Y';

		$link = $APPLICATION->GetPopupLink([
			'URL' => $urlForAddNew
		]);

		$buttonForAddNew = [
			"URL" => 'javascript:'.$link,
			'TEXT' => 'Добавить элемент',
			'TITLE' => 'Добавить элемент',
			'ICON' => 'bx-context-toolbar-create-icon',
			"ID" => "bx-context-toolbar-add-element",
		];

		$this->addIncludeAreaIcon(
			$buttonForAddNew
		);

		$this->includeComponentTemplate();
	}
}
