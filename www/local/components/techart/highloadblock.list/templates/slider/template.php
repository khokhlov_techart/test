<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) { die(); } ?>

<section>
	<?php foreach ($arResult['rows'] as $row) {?>
		<?php
		$this->AddEditAction(
			$row['ID'],
			"/bitrix/admin/highloadblock_row_edit.php?ENTITY_ID=" . $arParams['BLOCK_ID'] . "&ID=" . $row['ID'] . "&lang=ru&bxpublic=Y",
			"Изменить " . $row['ID'],
			Array(
				"WINDOW" => array("wight"=>780, "height"=>500),
				"ICON" => "bx-context-toolbar-edit-icon",
				"SRC" => "/bitrix/images/myicon.gif"
			)
		);
		?>
		<article id="<?= $this->GetEditAreaId($row['ID']); ?>">
			<?php foreach($arResult['tableColumns'] as $col => $bool) { ?>
				<?= $row[$col] ?>
			<?php } ?>
		</article>
	<?php } ?>
</section>
