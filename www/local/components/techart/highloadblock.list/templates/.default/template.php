<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) { die(); } ?>

<div class="reports-result-list-wrap">
	<div class="report-table-wrap">
		<div class="reports-list-left-corner"></div>
		<div class="reports-list-right-corner"></div>
		<table cellspacing="0" class="reports-list-table" id="report-result-table">
			<!-- head -->
			<tr>
				<?php
				$i = 0;
				foreach(array_keys($arResult['tableColumns']) as $col) {
					$i++;

					if ($i == 1) {
						$th_class = 'reports-first-column';
					} else if ($i == count($arResult['viewColumns'])) {
						$th_class = 'reports-last-column';
					} else {
						$th_class = 'reports-head-cell';
					}

					// title
					$arUserField = $arResult['fields'][$col];
					$title = $arUserField["LIST_COLUMN_LABEL"] ? $arUserField["LIST_COLUMN_LABEL"] : $col;

					// sorting
					$defaultSort = 'DESC';
					//$defaultSort = $col['defaultSort'];

					if ($col === $arResult['sort_id']) {
						$th_class .= ' reports-selected-column';

						if($arResult['sort_type'] == 'ASC') {
							$th_class .= ' reports-head-cell-top';
						}
					} else {
						if ($defaultSort == 'ASC') {
							$th_class .= ' reports-head-cell-top';
						}
					}
					?>

					<th class="<?= $th_class ?>" colId="<?= htmlspecialcharsbx($col) ?>" defaultSort="<?= $defaultSort ?>">
						<div class="reports-head-cell">
							<?php if ($defaultSort) { ?>
								<span class="reports-table-arrow"></span>
							<? } ?>
							<span class="reports-head-cell-title">
								<?= htmlspecialcharsex($title) ?>
							</span>
						</div>
					</th>
				<?php } ?>
			</tr>

			<!-- data -->
			<?php foreach ($arResult['rows'] as $row) { ?>
			<?php
			$this->AddEditAction(
				$row['ID'],
				"/bitrix/admin/highloadblock_row_edit.php?ENTITY_ID=" . $arParams['BLOCK_ID'] . "&ID=" . $row['ID'] . "&lang=ru&bxpublic=Y",
				"Изменить " . $row['ID'],
				Array(
					"WINDOW" => array("wight"=>780, "height"=>500),
					"ICON" => "bx-context-toolbar-edit-icon",
					"SRC" => "/bitrix/images/myicon.gif"
				)
			);
			?>

			<tr class="reports-list-item" id="<?= $this->GetEditAreaId($row['ID']); ?>">
				<?php
				$i = 0;
				foreach(array_keys($arResult['tableColumns']) as $col) {
					$i++;
					if ($i == 1) {
						$td_class = 'reports-first-column';
					} else if ($i == count($arResult['viewColumns'])) {
						$td_class = 'reports-last-column';
					} else {
						$td_class = '';
					}

					$finalValue = $row[$col];

					?>
					<td class="<?=$td_class?>">
						<?=$finalValue?>
					</td>
				<?php } ?>
				</tr>
			<?php } ?>

		</table>

		<?php
		if ($arParams['ROWS_PER_PAGE'] > 0) {
			$APPLICATION->IncludeComponent(
				'bitrix:main.pagenavigation',
				'',
				array(
					'NAV_OBJECT' => $arResult['nav_object'],
					'SEF_MODE' => 'N',
				),
				false
			);
		}
		?>


		<form id="hlblock-table-form" action="" method="get">
			<input type="hidden" name="BLOCK_ID" value="<?= htmlspecialcharsbx($arParams['BLOCK_ID']) ?>">
			<input type="hidden" name="sort_id" value="">
			<input type="hidden" name="sort_type" value="">
		</form>

		<script type="text/javascript">
			BX.ready(function(){
				var rows = BX.findChildren(BX('report-result-table'), {tag:'th'}, true);
				for (i in rows)
				{
					var ds = rows[i].getAttribute('defaultSort');
					if (ds == '') {
						BX.addClass(rows[i], 'report-column-disabled-sort')
						continue;
					}

					BX.bind(rows[i], 'click', function(){
						var colId = this.getAttribute('colId');
						var sortType = '';

						var isCurrent = BX.hasClass(this, 'reports-selected-column');

						if (isCurrent) {
							var currentSortType = BX.hasClass(this, 'reports-head-cell-top') ? 'ASC' : 'DESC';
							sortType = currentSortType == 'ASC' ? 'DESC' : 'ASC';
						} else {
							sortType = this.getAttribute('defaultSort');
						}

						var idInp = BX.findChild(BX('hlblock-table-form'), {attr:{name:'sort_id'}});
						var typeInp = BX.findChild(BX('hlblock-table-form'), {attr:{name:'sort_type'}});

						idInp.value = colId;
						typeInp.value = sortType;

						BX.submit(BX('hlblock-table-form'));
					});
				}
			});
		</script>
	</div>
</div>