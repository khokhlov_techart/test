<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

class CHighloadblockTools
{
	public static function getFieldData($fieldType, $rawValue)
	{
		$method = self::toMethodName($fieldType);
		$result = call_user_func([__CLASS__, $method], $rawValue);
		return $result ? $result : $rawValue;
	}

	protected static function toMethodName($fieldType) {
		return "uf_type_{$fieldType}";
	}

	protected static function uf_type_file($id)
	{
		if ($id) {
			return CFile::GetFileArray($id);
		}
		return null;
	}
}
?>
