<?php
	$email = $email ?? '';
	$phone = $phone ?? '';
?>

<div class="<?php echo e($block); ?>">
	<div class="<?php echo e($block->elem('email')); ?>"><?php echo $email; ?></div>
	<div class="<?php echo e($block->elem('phone')); ?>"><?php echo $phone; ?></div>
</div><?php /**PATH /var/www/workspace/test-project/www/local/templates/site/frontend/src/block/common/footer/footer.blade.php ENDPATH**/ ?>